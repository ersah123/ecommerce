// config used by dashboard client side only
const prodHost = process.env.NODE_HOST || 'localhost';

module.exports = {
  // dashboard UI language
  language: 'en',
  apiBaseUrl: `http://${prodHost}:3001/api/v1`,
  apiWebSocketUrl: `ws://${prodHost}:3001`,
  developerMode: true
}
