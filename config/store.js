// config used by store client side only
const prodHost = process.env.NODE_HOST || 'localhost';

module.exports = {
  // store UI language
  language: 'en',
  ajaxBaseUrl: `http://${prodHost}:3001/ajax`
}
