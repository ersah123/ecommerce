# Use an official Python runtime as a parent image
FROM node

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN npm install

# Make port 80 available to the world outside this container
EXPOSE 3000
EXPOSE 27017

# Run app.py when the container launches
CMD ["npm", "install"]
CMD ["npm", "run build"]
CMD ["npm", "start"]
